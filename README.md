# element-editor

> 基于Vue 搭建的一套可视化页面编辑器，组件化编辑网页，轻松生成Vue页面。
> 使用饿了么开源的 elementUI 作为基础组件

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
